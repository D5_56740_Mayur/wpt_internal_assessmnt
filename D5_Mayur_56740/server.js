const express = require('express')
const mysql = require('mysql')

function openDatabaseConnection() {
 
  const connection = mysql.createConnection({
    
    host: 'localhost',
   
    port: 3306,
 
    user: 'root',
    
    password: 'Mayur12345',
   
    database: 'dec10',
  })

 
  connection.connect()

  return connection
}

const app = express()

app.get('/', (request, response) => {
  response.send('welcome to Student details')
})
app.get('/Student', (request, response) => {
    
    const connection = openDatabaseConnection()
  
   
    const statement = `select roll_no, name, class, division, dateofbirth,parent_mobile_no from Student`
  //roll_no | name  | class | division | dateofbirth | parent_mobile_no
    // execute the query
    connection.query(statement, (error, data) => {
    
      connection.end()
  
      if (error) {
     
        response.send(error)
      } else {
    
        response.send(data)
      }
    })
  })
  app.get('/Studentid', (request, response) => {
    const connection = openDatabaseConnection()
   // Find Student by rollno 
     const statement = `select roll_no, name, class, division, dateofbirth,parent_mobile_no from Student where roll_no=1`
   connection.query(statement, (error, data) => {
     connection.end()
      if (error) {
    response.send(error)
       } else {
      response.send(data)
       }
     })
   })
   app.get('/Studentbyclass', (request, response) => {
    const connection = openDatabaseConnection()
   // Find Student by class
     const statement = `select roll_no, name, class, division, dateofbirth,parent_mobile_no from Student where class="DAC"`
   connection.query(statement, (error, data) => {
     connection.end()
      if (error) {
    response.send(error)
       } else {
      response.send(data)
       }
     })
   })
   app.get('/Studentbybirthyear', (request, response) => {
    const connection = openDatabaseConnection()
   // Find Student by class
     const statement = `select roll_no, name, class, division, dateofbirth,parent_mobile_no from Student where dateofbirth BETWEEN '1997-01-01' AND '1997-12-31' `
   connection.query(statement, (error, data) => {
     connection.end()
      if (error) {
    response.send(error)
       } else {
      response.send(data)
       }
     })
   })
   app.delete('/Studentdelete', (request, response) => {
    const connection = openDatabaseConnection()
   // Find Student by class
     const statement = `delete from student where roll_no=3`
   connection.query(statement, (error, data) => {
     connection.end()
      if (error) {
    response.send(error)
       } else {
      response.send(data)
       }
     })
   })
   app.post('/Studentadd', (request, response) => {
    const connection = openDatabaseConnection()
   // Find Student by class
     const statement = `insert into Student values(3,"Pk","DAC","D4","1997-12-22",9963)`
   connection.query(statement, (error, data) => {
     connection.end()
      if (error) {
    response.send(error)
       } else {
      response.send(data)
       }
     })
   })
   app.post('/Studentedit', (request, response) => {
    const connection = openDatabaseConnection()
   // Find Student by class
     const statement = `update student set class="Pharmacy",division="p1" where roll_no=2`
     // Edit specific Student's class and division 
     //update student set name="Pramod" where roll_no=4;
   connection.query(statement, (error, data) => {
     connection.end()
      if (error) {
    response.send(error)
       } else {
      response.send(data)
       }
     })
   })
   app.listen(4001, () => {
    console.log('server started on port 4001')
  })